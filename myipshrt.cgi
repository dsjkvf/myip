#!/usr/bin/env python3
# coding: utf-8

import cgi
import os
import json
from urllib.request import urlopen
from myip import countries

print("Content-type: text/html\n\n")
print('<head>')
print('</head>')
print('<body>')

your_ip = cgi.escape(os.environ['REMOTE_ADDR'])
your_ua = cgi.escape(os.environ['HTTP_USER_AGENT'])

url = 'http://ipinfo.io/' + your_ip + '/json'
data = json.load(urlopen(url))
your_country_code = data['country']
your_country = countries.get(your_country_code)

print(your_ip + ' // ' + your_country + ' // ' + your_ua)
