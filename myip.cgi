#!/usr/bin/env python3

import cgi
import os
import json
from urllib.request import urlopen

your_ip = cgi.escape(os.environ['REMOTE_ADDR'])
your_ua = cgi.escape(os.environ['HTTP_USER_AGENT'])

url = 'http://ipinfo.io/' + your_ip + '/json'
response = urlopen(url)
data = json.load(response)

your_provider = data['org']
your_city = data['city']
if your_city == 'Aikstieji Versupiai':
    your_city = 'Vilnius'
your_region = data['region']
your_country = data['country']

print("Content-type: text/html\n\n")
print('<head>')
print('<meta http-equiv="refresh" content="150">')
print('<title>' + your_country + ': ' + your_ip + '</title>')
print('<link rel="stylesheet" href="myip.css" type="text/css">')
print('</head>')
print('<body>')

print('<div class="myip">')
print('<h2>Your details<a style="text-decoration:none;color:black;" href="https://duckduckgo.com/">:</a></h2>')
print('<br>')
print('<strong>IP:</strong> ' + your_ip)
print('<br>')
print('<strong>User-Agent:</strong> ' + your_ua)
print('<br>')
if your_provider != '':
    print('<strong>Provider:</strong> ' + your_provider)
    print('<br>')
if your_city != '':
    print('<strong>City:</strong> ' + your_city)
    print('<br>')
if your_region != '':
    print('<strong>Region:</strong> ' + your_region)
    print('<br>')
if your_country != '':
    print('<strong>Country:</strong> ' + your_country)
print('</div>')
print('<div class="myspeed">')
print('<table width=100% height=100% cellpadding=0 cellspacing=0 border=0>')
print('<tr height=100%>')
print('''<td colspan=3 align=center valign=middle onclick="location='myspeed.html'">&nbsp;</td>''')
print('</tr>')
print('</table>')
print('</div>')
print('</body>')
print('</html>')
